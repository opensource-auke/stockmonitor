FROM --platform=$BUILDPLATFORM python:3.9

ARG TARGETPLATFORM
ARG BUILDPLATFORM

LABEL org.opencontainers.image.authors=aukevanderheide@gmail.com
LABEL org.opencontainers.image.url=https://gitlab.com/groups/opensource-auke/-/container_registries/3094507
LABEL org.opencontainers.image.documentation=https://gitlab.com/opensource-auke/stockmonitor/-/wikis/home
LABEL org.opencontainers.image.source="todo"
LABEL org.opencontainers.image.version=1.0
LABEL org.opencontainers.image.revision=1
LABEL org.opencontainers.image.licenses='MIT'
LABEL org.opencontainers.image.title=StockMonitor
LABEL org.opencontainers.image.description='Check stocks and retrieves news if up or down in Discord'

WORKDIR /usr/src/app

COPY ../src/requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY ../src/config.json .
COPY ../src/*.py .

CMD ["python", "./main.py"]

# Testing for Multi Platform
RUN echo "I am running on $BUILDPLATFORM, building for $TARGETPLATFORM" > /log