# StockMonitor

Welcome to my [Stockmonitor](https://gitlab.com/opensource-auke/stockmonitor) project.

[[_TOC_]]


## What's it for

This app checks your stocks yesterday closing value with the day before. If the value increased or decreased by a given percentage (can be different for each stock) it fetches news items from the company and sends a discord message with the percentage increase or decrease and links to the news items.

Because it uses *webhooks* for sending to **discord** there is no need to register a bot in **discord**.

## Status

**Released [v1.0.0](%v1.0.0)**

[Sources](https://gitlab.com/opensource-auke/stockmonitor/-/tree/main)

[Issues](https://gitlab.com/opensource-auke/stockmonitor/-/issues)

[Docker container on GitLab](https://gitlab.com/groups/opensource-auke/-/container_registries/3094507)

[Docker container on Docker Hub](https://hub.docker.com/repository/docker/aukevanderheide/stockmonitor)

## Credits

This program is based on an exercise from the [100 Days of Code - The Complete Python Pro Bootcamp](https://www.udemy.com/course/100-days-of-code) Udemy course from Angela Yu.


## Setup

Before you can use this, you have to create a *config.json* file in the *src/* folder.

For this you can copy the *config.json.sample* to *config.json* and edit the file.

## Edit config.json

### alphavantage_api

A string containing your api key from alphavantage.

You can get your free API key [here](https://www.alphavantage.co/)

### news_api

A string containing your api key from news.

You can get your free API key [here](https://newsapi.org/)

### nr_of_news_articles

The number of news articles you want to show up in the discord message. News articles are only fetched if the stock increased or decreased by the given percentage.

### discord_webhook

The webhook from the channel you want to send the message to.

### stocks

For each stock you want to monitor, there should be an entry. You can add as many stocks as you like.

#### name

The name of the stock, used to find the stock values. You can find the names at https://www.alphavantage.co/query?function=SYMBOL_SEARCH

#### company

The name of the company, used to find news about the company

#### difference

The percentage (increase or decrease) that should trigger a message.

## Use

### Bare metal

You must have **python3** installed.

Install the requirements for the app

```bash
cd src
pip3 install -r requirements.txt
```

Run the app

```bash
python3 main.py
```
### Docker run

```bash
docker run --rm \
  -it \
  --name stockmonitor \
  -v "$PWD/src/config.json":/usr/src/app/config.json \
  registry.gitlab.com/opensource-auke/stockmonitor/stockmonitor:latest
```

### Docker Swarm and Docker compose (preferred!)

You have to take care that the app starts (at least) once a day. You can do this with an *internal* (from [v1.0.1](%v1.0.1)**) or an *external* cronjob tool.

#### External Cronjob tool

```yaml
version: '3.7'

x-defaults-opts:
  &default-opts
  logging:
    options:
      max-size: "1m"

services:

  cronjob:
    image: crazymax/swarm-cronjob:1.10.0
    networks:
      - backend
    volumes:
      - "/var/run/docker.sock:/var/run/docker.sock"
    environment:
      - "TZ=Europe/Amsterdam"   # Timezone to use
      - "LOG_LEVEL=info"
      - "LOG_JSON=false"
    deploy:
      placement:
        constraints:
          - node.role == manager    # Must run on a manager node

  stockmonitor:
    <<: *default-opts
    image: registry.gitlab.com/opensource-auke/stockmonitor/stockmonitor:latest
    networks:
      - backend
    configs:
      - source: stockmonitor.conf.v0
        target: /usr/src/app/config.json
    deploy:
      replicas: 0
      update_config:
        failure_action: rollback
      restart_policy:
        condition: none 
      labels:
        - "swarm.cronjob.enable=true"
        - "swarm.cronjob.schedule=00 8 * * *"  # Every day on 8:00 AM
        - "swarm.cronjob.skip-running=true"  

networks:

  backend:
    driver: bridge
    name: backend
    attachable: true

# if config change than name must be changed (.vX)
configs:

  stockmonitor.conf.v0:
    file: ./src/config.json
```

#### Internal Schedule

Update your config json add a value for *run_every*

```json
"run_every": 1
```
```yaml
version: '3.7'

x-defaults-opts:
  &default-opts
  logging:
    options:
      max-size: "1m"

services:
        -
  stockmonitor:
    <<: *default-opts
    image: registry.gitlab.com/opensource-auke/stockmonitor/stockmonitor:latest
    networks:
      - backend
    configs:
      - source: stockmonitor.conf.v1
        target: /usr/src/app/config.json
    deploy:
      replicas: 1
      update_config:
        failure_action: rollback
      restart_policy:
        condition: none

networks:

  backend:
    driver: bridge
    name: backend
    attachable: true

# if config change than name must be changed (.vX)
configs:

  stockmonitor.conf.v1:
    file: ./src/config.json
```

for Docker-compose

```bash
docker compose up -d
```