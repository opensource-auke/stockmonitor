#!/usr/bin/env bash
# build the container and push it to the gitlab repository

app=stockmonitor
version=1.0.2-dev

if [ $# -ne 1 ]
then
  echo "Need 1 argument"
  echo "platform: gitlab docker all split arm"
  read platform

else
  platform=$1
fi

echo "Build and push version ($version) to platform ($platform)"

case $platform in

  docker)
  echo "Login to docker"
  docker login 1>/dev/null 2>/dev/null || {echo "Not logged in to docker" exit 1}

  echo "Pushing to docker"
  docker buildx build \
  --no-cache \
  --push \
  --platform linux/arm/v6,linux/arm/v7,linux/arm64/v8,linux/amd64 \
  --tag aukevanderheide/$app:$version \
  --tag aukevanderheide/$app:latest .
  ;;

  gitlab)
  echo "Login to gitlab"
  docker login registry.gitlab.com 1>/dev/null 2>/dev/null || {echo "Not logged in to gitlab" exit 1}

  echo "Pushing to gitlab"
  docker buildx build \
  --no-cache \
  --push \
  --platform linux/arm/v7,linux/arm64/v8,linux/amd64 \
  --tag registry.gitlab.com/opensource-auke/stockmonitor/$app:$version \
  --tag registry.gitlab.com/opensource-auke/stockmonitor/$app:latest .
  ;;

  all)
  echo "Login to docker"
  docker login 1>/dev/null 2>/dev/null || {echo "Not logged in to docker" exit 1}

  echo "Login to gitlab"
  docker login registry.gitlab.com 1>/dev/null 2>/dev/null || {echo "Not logged in to gitlab" exit 1}

  echo "Pushing to gitlab and docker"
  docker buildx build \
  --no-cache \
  --push \
  --platform linux/arm/v7,linux/arm64/v8,linux/amd64 \
  --tag aukevanderheide/$app:$version \
  --tag aukevanderheide/$app:latest \
  --tag registry.gitlab.com/opensource-auke/stockmonitor/$app:$version \
  --tag registry.gitlab.com/opensource-auke/stockmonitor/$app:latest .
  ;;

  split)
  echo "Login to gitlab"
  docker login registry.gitlab.com 1>/dev/null 2>/dev/null || {echo "Not logged in to gitlab" exit 1}
  echo "Pushing to gitlab and split for each platform"

  docker buildx build \
  --no-cache \
  --push \
  --platform linux/arm/v7 \
  --tag registry.gitlab.com/opensource-auke/stockmonitor/$app-arm:$version \
  --tag registry.gitlab.com/opensource-auke/stockmonitor/$app-arm:latest .

    docker buildx build \
  --no-cache \
  --push \
  --platform linux/arm64/v8 \
  --tag registry.gitlab.com/opensource-auke/stockmonitor/$app-arm64:$version \
  --tag registry.gitlab.com/opensource-auke/stockmonitor/$app-arm64:latest .

    docker buildx build \
  --no-cache \
  --push \
  --platform linux/amd64 \
  --tag registry.gitlab.com/opensource-auke/stockmonitor/$app-amd64:$version \
  --tag registry.gitlab.com/opensource-auke/stockmonitor/$app-amd64:latest .
  ;;

  arm)
  echo "Login to gitlab"
  docker login registry.gitlab.com 1>/dev/null 2>/dev/null || {echo "Not logged in to gitlab" exit 1}
  echo "Pushing to gitlab for platform arm"

  docker buildx build \
  --no-cache \
  --push \
  --platform linux/arm/v7 \
  --tag registry.gitlab.com/opensource-auke/stockmonitor/$app-arm:$version \
  --tag registry.gitlab.com/opensource-auke/stockmonitor/$app-arm:latest .
  ;;

  *)
  echo "Unknown platform: See ./build.sh for options"
  ;;
esac