import requests

class Alphavantage():
    def __init__(self, api_key):
        self.api_key = api_key
        self.url = 'https://www.alphavantage.co/query'

    def get_stock(self, name):

        params = {
            "function": "TIME_SERIES_DAILY",
            "symbol": name,
            "apikey": self.api_key,
            "outputsize": "compact",
            "datatype": "json"
        }

        response = requests.get(url=self.url, params=params)
        data = response.json()["Time Series (Daily)"]
        return(data)