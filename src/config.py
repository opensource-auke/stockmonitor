import logging
import json
import sys


class Config:

    def __init__(self, config_file):
        self.config = {}
        try:
            with open(config_file) as file:
                self.config = json.load(file)
        except Exception as e:
            logging.critical(f'''No config.json found. Error: {str(e)}''')
            sys.exit(1)

    def get(self, key_name, required=True):
        result = self.get_sub(key_name=key_name, config_name=self.config, required=required)
        return result


    def get_sub(self, key_name, config_name, required=True):
        try:
            result = config_name[key_name]
        except KeyError as e:
            if required:
                logging.critical(f'''Key not found in config.json. Error: {str(e)}''')
                sys.exit(1)
            else:
                return False
        else:
            return result
