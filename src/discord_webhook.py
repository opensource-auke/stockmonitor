from discord import Webhook, RequestsWebhookAdapter, Embed

class Discord_Webhook():
    def __init__(self, url):
        self.url = url
        self.message = {}

    def send_message(self):
        webhook = Webhook.from_url(url=self.url, adapter=RequestsWebhookAdapter())
        embed_message = Embed(title=self.message["title"], description=self.message["description"])
        for field in self.message["fields"]:
            embed_message.add_field(name=field["name"], value=field["value"])
        webhook.send(content=None, embed=embed_message)
        self.message = {}

    def new_message(self, title, description):
        self.message = {
            "title": title,
            "description": description,
            "fields": []
        }
        print(f'''Created new message: {self.message}''')


    def append_message(self, name, value):
          add_message = {
              "name": name,
              "value": value
          }
          self.message["fields"].append(add_message)