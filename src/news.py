import requests

class News():
    def __init__(self, api_key):
        self.api_key = api_key
        self.url = 'https://newsapi.org/v2/everything'

    def get_news(self, name, amount):
        params = {
            "qInTitle": name,
            # "from": date.today(),
            "sortBy": "publishedAt",
            "apiKey": self.api_key,
        }

        response = requests.get(url=self.url, params=params)
        articles = response.json()["articles"]
        total_results = response.json()["totalResults"]
        # print(f'''Total results :  {total_results} found!''')

        if total_results >= amount:
          data = articles[:amount]
        elif total_results > 0:
            data = articles[:total_results]
        else:
            data = [
                {
            "source": {
                "id": None,
                "name": "None"
            },
            "author": "stock-monitor",
            "title": "No News found",
            "description": "No news found for this company!",
            "url": None,
            "urlToImage": None,
            "publishedAt": None,
            "content": "the API dit not return any news articles for this company!"
                }
            ]
        return data