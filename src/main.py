import json
import logging
import time

from alphavantage import Alphavantage
from discord_webhook import Discord_Webhook
from news import News
from config import Config

VERSION = "1.0.2-dev"
APP = "stock-monitor"

# Initialise config and logging
LOG_LEVEL = logging.INFO  # default is logging.WARNING
LOG_FORMAT = "%(asctime)s %(levelname)s %(message)s"
logging.basicConfig(format=LOG_FORMAT, level=LOG_LEVEL)

config = Config('./config.json')
stock_data = Alphavantage(config.get('alphavantage_api'))
webhook = Discord_Webhook(config.get('discord_webhook'))
news = News(config.get('news_api'))
RUN_EVERY = config.get('run_every', required=False)
if not RUN_EVERY:
    RUN_EVERY = 0


def check_stocks():
    for stock in config.get('stocks'):
        try:
            data = stock_data.get_stock(stock['name'])
        except KeyError:
            logging.error(f'''Stock {stock["name"]} does not exist''')
        else:
            logging.info(f'''Stock {stock["name"]}''')
            data_list = [value for (key, value) in data.items()]

            yesterday_data = data_list[0]
            yesterday_closing_price = float(yesterday_data["4. close"])
            logging.info(f'''Yesterday Closing price {yesterday_closing_price}''')

            day_before_yesterday_data = data_list[1]
            day_before_yesterday_closing_price = float(day_before_yesterday_data["4. close"])
            logging.info(f'''Day before closing price {day_before_yesterday_closing_price}''')

            difference = yesterday_closing_price - day_before_yesterday_closing_price
            up_down = None
            if difference > 0:
                up_down = "🔺"
            else:
                up_down = "🔻"
            diff_percent = round((difference / yesterday_closing_price) * 100.0)
            logging.info(f'''Stock is {abs(diff_percent)}% {up_down}''')
            if abs(diff_percent) > stock["difference"]:
                logging.info(f'''Get news for {stock["company"]}''')
                try:
                    articles = news.get_news((stock["company"]), config.get("nr_of_news_articles"))
                except KeyError:
                    logging.warning(f'''No articles for company {stock["company"]} found!''')
                # except TypeError:
                    print('TypeError')
                else:
                    logging.info(articles)
                    webhook.new_message(f'''{APP} v:{VERSION}''',
                                        f'''{stock["company"]} - {stock["name"]}: {up_down} {abs(diff_percent)} %''')
                    for article in articles:
                        webhook.append_message(article["title"], article["url"])
                    webhook.send_message()


if __name__ == '__main__':

    while True:
        logging.basicConfig(level=logging.INFO)  # for testing, if released remove or set to Warning.
        logging.info(f'''Starting {APP} v:{VERSION}''')
        webhook.new_message(f'''{APP} v:{VERSION}''',"Checking your stocks")
        for monitor_stock in config.get('stocks'):
            webhook.append_message(name=monitor_stock["company"],
                                   value=f'''Stock {monitor_stock["name"]}\nDifference > {monitor_stock["difference"]} %''')    # NOQA: E501
        webhook.send_message()
        check_stocks()
        if RUN_EVERY == 0:
            break
        else:
            time.sleep(RUN_EVERY * 60 * 60)  # RUN_EVERY times 60 Minutes times 60 Seconds
