#!/bin/bash
# run docker container

app=stockmonitor
version=1.0.2-dev

docker run --rm \
  -it \
  --name $app \
  -v ./src/config.json:/usr/src/app/config.json
  registry.gitlab.com/opensource-auke/stockmonitor/$app:$version
